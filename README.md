# Record-bot

Record Bot is bot designed to Discord VoIP software.

## What?
Record-bot is designed to fetch albums from Discogs.com record database. In the first version user can get random record via command issued to bot.

Later on (if possible) user can narrow the record fetching with specific genre of music. 

# Contributing
Please refer to each project's style and contribution guidelines for submitting patches and additions. 

- Fork the repo on GitHub
- Clone the project to your own machine
- Commit changes to your own branch
- Push your work back up to your fork
- Submit a *Merge request so that we can review your changes

**NOTE: Be sure to merge the latest from "upstream" before making a pull request!**

# Maintainer
Tommi Siik

# License
MIT